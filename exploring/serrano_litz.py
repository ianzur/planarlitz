"""
Spiral (half circle method) litz wire for planar inductor

python implementation of algorithm described in:
@INPROCEEDINGS{8217216,
  author={Serrano, J. and Lope, I. and Acero, J. and Carretero, C. and Burdío, J.M.},
  booktitle={IECON 2017 - 43rd Annual Conference of the IEEE Industrial Electronics Society}, 
  title={Mathematical description of PCB-adapted litz wire geometry for automated layout generation of WPT coils}, 
  year={2017},
  volume={},
  number={},
  pages={6955-6960},
  doi={10.1109/IECON.2017.8217216}}

slight modifications used to transform from matlab (1 based) to python (0 based) indexing

"""
import math
from typing import Union

import matplotlib.pyplot as plt
import numpy as np

def turn_width(num_strands: int, strand_width: float, strand_pitch: float, turn_clearance: float):
    """width of a turn (eq. 1)

    Args:
        num_strands (int): number of strands
        strand_width (float): width of a strands
        strand_pitch (float): pitch of strands

    Returns:
        float: the width of a turn

    Raises:
        ValueError: when num_strands is even
    """

    if num_strands % 2 == 0:
        raise ValueError("argument \"num_strands\" must be odd")

    n_superior = int((num_strands + 1) / 2) # eq. 2
    n_inferior = n_superior - 1 # eq. 3 (slightly changed)

    # slightly changed from original implementation to use pitch instead of "strand clearance"
    turn_width = strand_pitch * n_inferior + strand_width + turn_clearance

    return turn_width


def strand_radii(external_radius: float, num_turns: int, num_strands: int, trace_width: float, trace_pitch: float, turn_clearance: float=0.0):
    """calc radii for every strand (algorithm 1)

    Args:
        external_radius (float): _description_
        num_turns (int): _description_
        num_strands (int): _description_
        trace_width (float): _description_
        trace_pitch (float): _description_
        turn_clearance (float, optional): _description_. Defaults to 0.0.

    Returns:
        np.ndarray: radius of every strand

    """

    # radii[0,turn,strand] = top_half_circle
    # radii[1,turn,strand] = bottom_half_circle
    radii = np.zeros((2, num_turns, num_strands))

    turnwidth = turn_width(num_strands, trace_width, trace_pitch, turn_clearance)

    for j in range(radii.shape[1]):
        radii[0, j, 0] = external_radius - (j * turnwidth)
        radii[1, j, 0] = radii[0, j, 0] - (turnwidth / 2) 

    for i in range(radii.shape[0]):
        for j in range(radii.shape[1]):
            for k in range(1, radii.shape[2]):
                
                # A divergence from the indexing scheme of the original paper.
                # 0 == external strand, odd == inferior strand 

                radii[i, j, k] = radii[i, j, 0] - k * (trace_pitch / 2)

    return radii


def via_placement(radii: np.ndarray, changes_in_turn: Union[list, int]):
    """via placement (algorithm 2)

    Args:
        radii (np.ndarray): _description_
        changes_in_turn (listlike or int): number of changes in each turn external turn is index 0

    Return:
        tuple(np.array, np.array): (radius, angle)

    Raises:
        ValueError: 
    """
    
    
    if isinstance(changes_in_turn, int):
        changes_in_turn = [changes_in_turn] * radii.shape[1]
    if radii.shape[1] != len(changes_in_turn):
        raise ValueError

    # index = turns
    via_loc = np.empty((radii.shape[1]), dtype=object)

    for turn, changes in enumerate(changes_in_turn):
        via_angles_in_turn = []
        via_radii_in_turn = []

        for i in range(changes):
            via_angle = math.pi / (2 * changes) + 2 * math.pi * (i / changes)

            if i % 2 == 0:
                if via_angle < math.pi:
                    via_radius = radii[0,turn,0]
                else:
                    via_radius = radii[1,turn,0]
            else:
                if via_angle < math.pi:
                    via_radius = radii[0,turn,-1]
                else:
                    via_radius = radii[1,turn,-1]

            via_angles_in_turn.append(via_angle)
            via_radii_in_turn.append(via_radius)

        via_loc[turn] = np.array([via_angles_in_turn, via_radii_in_turn])

    return via_loc


def trace_angles(via_loc, a, b, y, num_strands):
    """track angle (algorithm 3)

    Args:
        via_angles (_type_): _description_
        a (_type_): _description_
        b (_type_): _description_
        y (_type_): _description_
        num_strands (_type_): _description_

    Returns:
        np.ndarray: start and end angles for each strand

    """
    
    via_locs = np.transpose(np.concatenate(via_loc, axis=1))
   
    # strand_angles[0,num_turns,num_strands] = start
    # strand_angles[1,num_turns,num_strands] = end
    strand_angles = np.empty((2,via_locs.shape[0],num_strands))

    # all strand angles start at 0
    strand_angles[0,0,:] = 0

    traces_superior = np.array(range(num_strands // 2 + 1))
    traces_inferior = np.array(range(num_strands // 2))

    # print(traces_superior)
    # print(traces_inferior)

    idx = 0
    for j, via in enumerate(via_locs[:,0]):

        for k in range(strand_angles.shape[2]):

            strand_angles[1,j,::2] = via - a - traces_superior * b
            strand_angles[1,j,1::2] = via + traces_inferior * b

    strand_angles[0,1:,:] = strand_angles[1,:-1,:]

    return strand_angles


def main():

    external_radius = 80
    num_turns = 4
    num_strands = 9
    strand_width = 0.5
    strand_pitch = 1.0
    turn_clearance = 0
    changes_in_turn = [8,6,5,4]

    
    # angle between via and nearest strand
    alpha = math.radians(1)
    # angle between strands
    beta = math.radians(1)
    # angluar separation between strands
    gamma = math.radians(3)

    radii = strand_radii(external_radius, num_turns, num_strands, strand_width, strand_pitch)
    # print(radii)

    via_loc = via_placement(radii, changes_in_turn)
    # print(via_loc)

    strand_angles = trace_angles(via_loc, alpha, beta, gamma, num_strands)

    turn_0 = strand_angles[:, :changes_in_turn[0], :]
    print(turn_0.shape)
    print(radii.shape)

    fig = plt.figure(figsize=(5,5))
    axes = plt.axes(polar=True)

    print(turn_0[:,0,1].shape)
    print(radii[0,0,1].repeat(2))

    for trace in range(turn_0.shape[-1]):

        axes.plot(turn_0[:,0,trace], radii[0,0,trace].repeat(2))

    plt.show()



if __name__ == "__main__":

    main()
