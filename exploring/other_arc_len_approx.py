
import matplotlib.pyplot as plt
import numpy as np

def change_in_theta(s, theta):

    return s / (1 + theta**2)**0.5

a = 3
b = 1 / (2 * np.pi)
turns = 3

s = 7 * 2* np.pi

max_theta = (a + turns) * 2 * np.pi

# starting theta
theta = a * 2 * np.pi

thetas = []

while theta < max_theta:
    theta += change_in_theta(s, theta)
    thetas.append(theta)

thetas = np.array(thetas) - turns * 2 * np.pi

print(thetas)

### polar ###
fig = plt.figure(figsize=(6,3))
ax0 = plt.subplot(121, polar=True, aspect="equal")

# calculated points
r_div = a + b * thetas
ax0.scatter(thetas, r_div, color="#a65628", zorder=2)

# spiral line
theta = np.arange(a * 2 * np.pi, (a + turns) * 2 * np.pi + 0.001, np.pi / 32)
r = b * theta
ax0.plot(theta, r, color="#377eb8", zorder=1)

plt.show()
