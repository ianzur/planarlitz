# This file is part of Planar Litz Generator.
#
# PlanarLitzGenerator is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Planar Litz Generator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Planar Litz Generator. If not, see <https://www.gnu.org/licenses/>.
#
# (c) 2022 by Ian Zurutuza, <ian.zurutuza@gmail.com>

import numpy as np


def rotate(points, radians: float, origin=(0, 0)) -> np.ndarray:
    """rotate point(s) around an origin

    Args:
        points (ArrayLike): points to rotate
        radians (float): angle to rotate points
        origin (ArrayLike): Default is (0,0)

    Returns:
        np.ndarray: rotated points
    """

    cos_angle = np.cos(radians)
    sin_angle = np.sin(radians)

    R = np.array([[cos_angle, -sin_angle], [sin_angle, cos_angle]])

    points = np.atleast_2d(points)
    origin = np.atleast_2d(origin)

    if np.array_equal(origin, np.array((0, 0))):
        return np.squeeze((R @ (points.T)).T)

    return np.squeeze((R @ (points.T - origin.T) + origin.T).T)


def find_arc_center(p1: tuple, p2: tuple, p3: tuple):

    p1 = np.atleast_2d(p1)
    p2 = np.atleast_2d(p2)
    p3 = np.atleast_2d(p3)
    p_temp = np.zeros(p1.shape)

    # prevent divide by zero by rotating points
    rot1_check = np.argwhere((p2[:, 0] - p1[:, 0]) == 0)
    rot2_check = np.argwhere((p3[:, 0] - p2[:, 0]) == 0)

    p_temp[rot1_check] = p1[rot1_check]
    p1[rot1_check] = p2[rot1_check]
    p2[rot1_check] = p3[rot1_check]
    p3[rot1_check] = p_temp[rot1_check]

    p_temp[rot2_check] = p3[rot2_check]
    p3[rot2_check] = p2[rot2_check]
    p2[rot2_check] = p1[rot2_check]
    p1[rot2_check] = p_temp[rot2_check]

    ma = (p2[:, 1] - p1[:, 1]) / (p2[:, 0] - p1[:, 0])
    mb = (p3[:, 1] - p2[:, 1]) / (p3[:, 0] - p2[:, 0])

    # arc check (make sure this not a straight line)
    arc_check = np.argwhere((mb - ma) == 0)
    if len(arc_check) > 0:
        raise RuntimeError(f"The following indices do not contian arcs\n{arc_check}")

    center_x = (
        ma * mb * (p1[:, 1] - p3[:, 1])
        + mb * (p1[:, 0] + p2[:, 0])
        - ma * (p2[:, 0] + p3[:, 0])
    ) / (2 * (mb - ma))
    center_y = np.zeros(center_x.shape)

    # choose substitution equation by ensure perpendicular bisector is not a vertical line
    mask = np.ones(center_x.shape[0], dtype=bool)
    mask[np.argwhere(ma == 0)] = False

    center_y[mask] = (-1 / mb[mask]) * (
        center_x[mask] - (p2[mask, 0] + p3[mask, 0]) / 2
    ) + (p2[mask, 1] + p3[mask, 1]) / 2
    center_y[~mask] = (-1 / ma[~mask]) * (
        center_x[~mask] - (p1[~mask, 0] + p2[~mask, 0]) / 2
    ) + (p1[~mask, 1] + p2[~mask, 1]) / 2

    center_x = np.atleast_2d(center_x)
    center_y = np.atleast_2d(center_y)

    return np.concatenate([center_x, center_y]).T


def angle_between_vectors(a, b):

    a = np.atleast_2d(a)
    b = np.atleast_2d(b)

    a_u = a / np.linalg.norm(a, axis=1)[:, None]
    b_u = b / np.linalg.norm(b, axis=1)[:, None]
    return np.arccos(np.clip(np.einsum("ij,ij->i", a_u, b_u), -1.0, 1.0))


def length_of_3point_arc(start, p2, end):

    start = np.atleast_2d(start)
    p2 = np.atleast_2d(p2)
    end = np.atleast_2d(end)

    center = find_arc_center(start, p2, end)
    angle = angle_between_vectors(start - center, end - center)

    r = np.linalg.norm(start - center, axis=1)

    return r * angle
