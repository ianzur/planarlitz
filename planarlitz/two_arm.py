# This file is part of Planar Litz Generator.
#
# PlanarLitzGenerator is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published 
# by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Planar Litz Generator is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Planar Litz Generator. If not, see <https://www.gnu.org/licenses/>.
# 
# (c) 2022 by Ian Zurutuza, <ian.zurutuza@gmail.com>

from primitive_shapes import Archimedean, rotate

import numpy as np
import matplotlib.pyplot as plt


class TwoArm:

    def __init__(self, num_turns):

        self.num_turns = num_turns

        self.spiral_a = Archimedean(2, 1)
        self.spiral_b = Archimedean(2, 1)

    def plot(self, pts_per_turn):

        armA = rotate(self.spiral_a.points(self.num_turns, pts_per_turn), np.pi / 4)
        armB = rotate(self.spiral_b.points(self.num_turns, pts_per_turn), 5 * np.pi / 4)

        fig = plt.figure(figsize=(6, 6))
        ax = plt.axes()

        plt.plot(armA[:,0], armA[:,1], color="b")
        plt.plot(armB[:,0], armB[:,1], color="g")

        plt.show()




if __name__ == "__main__":

    antenna = TwoArm(4)

    antenna.plot(4)

    