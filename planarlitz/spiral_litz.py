# This file is part of Planar Litz Generator.
#
# PlanarLitzGenerator is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published 
# by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Planar Litz Generator is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Planar Litz Generator. If not, see <https://www.gnu.org/licenses/>.
# 
# (c) 2022 by Ian Zurutuza, <ian.zurutuza@gmail.com>

from pathlib import Path

import KicadModTree as kcmod
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import fsolve, root_scalar

from planarlitz.primitive_shapes import ArchimedeanSpiral
from geometry import find_arc_center, length_of_3point_arc

class SpiralLitz:
    
    def __init__(
        self,
        num_traces: int,
        num_turns: int,
        outer_diameter: float,
        num_sections: int = 60,
        # distance_between_changes: float = None,
        trace_width: float = 0.5,
        trace_pitch: float = 0.8,
        via_clearance: float = 0.1524,
        turn_pitch: float = None,
        alpha: float = 3,
        beta: float = 1.5,
    ):

        if num_traces % 2 == 0:
            raise ValueError(f"num_traces must be odd")

        self.outer_diameter = outer_diameter
        self.num_sections = num_sections

        # trace info
        self.num_traces = num_traces
        self.trace_width = trace_width
        self.trace_pitch = trace_pitch
        self.via_clearance = via_clearance

        # turn info
        self.num_turns = num_turns
        self.turn_width = (self.num_traces // 2) * self.trace_pitch + self.trace_width 
        self.turn_pitch = self.turn_width + self.trace_pitch / 2 if turn_pitch is None else turn_pitch

        # TODO: calculate to enforce clearance
        self.alpha = np.radians(alpha)
        self.beta = np.radians(beta)

        self.middle_spiral_index = num_traces // 2
        self.spirals = self.create_spirals()

        self.via_locations = self.calc_via_locations()

        # np.array 
        self.sub_arcs = self.calc_sub_arcs()

        # self.sub_arcs_cartesian = 
        # self.bridges = self.connect_sub_arcs()

    def create_spirals(self) -> list:
        """create each of the spirals that make up this inductor"""
        spirals = []

        inner_radius = self.outer_diameter / 2 - self.turn_width * (self.num_turns + 1) - self.trace_width
        step_outwards = self.turn_pitch / (2 * np.pi)

        for i in range(self.num_traces):
            spirals.append(ArchimedeanSpiral(inner_radius, step_outwards))
            inner_radius += self.trace_pitch / 2
        return spirals

    def calc_via_locations(self) -> list:
        """Using the central spiral to calculate the location (theta) for each via"""

        middle_spiral = self.spirals[self.middle_spiral_index]

        # get the entire length of the spiral
        total_length = middle_spiral.arc_length(0, self.num_turns * 2 * np.pi)
        # print(total_length)
        
        arc_length = total_length / (self.num_sections)

        # init_theta = self.num_turns * 2 * np.pi / (self.num_sections + 1)
        # init_thetas = np.cumsum(np.repeat(init_theta, self.num_sections + 1))[:-1]
        
        # print(min(init_thetas), max(init_thetas), self.num_sections == len(init_thetas))

        print(arc_length)

        max_theta = self.num_turns * 2 * np.pi
        theta = 0
        via_locations = []

        for i in range(self.num_sections-1):
            theta = root_scalar(self._optimize_via_placement, args=(theta, middle_spiral, arc_length), x0=theta, x1=max_theta).root
            via_locations.append(theta)

        via_locations = np.array(via_locations)
        return via_locations

    def _optimize_via_placement(self, theta_b, theta_a, spiral, arc_length):
        return spiral.arc_length(theta_a, theta_b) - arc_length

    def calc_sub_arcs(self) -> np.ndarray:
        """calculate the layer(top=0 or bottom=1), start, mid-point, and end for each sub-arc"""
        starts = np.zeros((self.num_sections, self.num_traces))
        midpoints = np.zeros((self.num_sections, self.num_traces))
        ends = np.zeros((self.num_sections, self.num_traces))
        layer = np.zeros((self.num_sections, self.num_traces))
        
        # initialize to spiral corresponding to index
        # middlepoint always corresponds to spiral by index
        start_spiral = np.tile(list(range(self.num_traces)), (self.num_sections, 1))
        end_spiral = np.tile(list(range(self.num_traces)), (self.num_sections, 1))

        # assign trace layers: 0 == bottom, 1 == top
        layer[::2, ::2] = 1
        layer[1::2, 1::2] = 1
       
        # assign via location to each start and stop
        starts[0, ...] = 0
        ends[-1, ...] = self.num_turns * 2 * np.pi

        # TODO:N't generalize the offset arrays
        # lets not do this offset and simply assign the spiral (by index) used to calculate each point
        # via on inside edge
        starts[1::2, ::2] = self.via_locations[::2, None] #- np.array([0,1,1.5,2,2.5]) * self.beta 
        ends[:-1:2, ::2] = self.via_locations[::2, None] #+ np.array([0,1,1.5,2,2.5]) * self.beta 
        starts[1::2, 1::2] = self.via_locations[::2, None] #+ np.array([1,1.5,2,2.5]) * (self.beta *1.5)
        ends[:-1:2, 1::2] = self.via_locations[::2, None] #- np.array([1,1.5,2,2.5]) * (self.beta * 1.5)

        start_spiral[1::2, 1::2] += 1
        end_spiral[:-1:2, 1::2] += 1
                
        # via on outside edge
        starts[2::2, ::2] = self.via_locations[1::2, None] #- np.array([2.5,2,1.5,1,0]) * self.beta
        ends[1:-1:2, ::2] = self.via_locations[1::2, None] #+ np.array([2.5,2,1.5,1,0]) * self.beta
        starts[2::2, 1::2] = self.via_locations[1::2, None] #+ np.array([2.5,2,1.5,1]) * (self.beta * 1.5)
        ends[1:-1:2, 1::2] = self.via_locations[1::2, None] #- np.array([2.5,2,1.5,1]) * (self.beta * 1.5)
   
        start_spiral[2::2, 1::2] -= 1
        end_spiral[1:-1:2, 1::2] -= 1

        midpoints = (starts + ends) / 2

        return np.stack([starts, midpoints, ends, layer, start_spiral, end_spiral])

    # def connect_sub_arcs(self):
    #     """bridge sub arcs together"""

    def plot(self):
        """show spiral in matplotlib -- used during development"""

        fig = plt.figure()
        ax = plt.axes(polar=True)

        # theta = np.arange(0, self.num_turns * 2 * np.pi + 0.001, np.pi / 64)

        for i, j in np.ndindex(self.sub_arcs.shape[1:]):
            if self.sub_arcs[2, i, j] == 1:
                color="#377eb8"
            else:
                color="#a65628"

            theta = np.linspace(self.sub_arcs[0, i, j], self.sub_arcs[1, i, j], 50, endpoint=True)
            ax.plot(theta, self.spirals[j].a + self.spirals[j].b * theta, color=color)

        via_radius = []
        for i, via in enumerate(self.via_locations):
            # spiral = self.spirals[self.middle_spiral_index]
            # print(via)
            if i % 2 == 0:
                spiral = self.spirals[0]
            else:
                spiral = self.spirals[-1]
            
            via_radius.append(spiral.a + spiral.b * via)

        plt.scatter(self.via_locations, via_radius, zorder=2)

        plt.show()

    def save(self, filename, path: Path=Path.cwd()):
        """save footprint as kicad_mod file

        Args:
            filename (string): filename, if it doesn't end with .kicad_mod it is appended.
            path (Path, optional): where to save the file. Defaults to current working directory.
        """

        if not filename.endswith(".kicad_mod"):
            filename += ".kicad_mod"

        if path is None:
            path = Path.cwd()

        footprint = kcmod.Footprint(filename.split('.')[0])

        for i, j in np.ndindex(self.sub_arcs.shape[1:]):
            s_spiral = int(self.sub_arcs[4,i,j])
            e_spiral = int(self.sub_arcs[5,i,j])
            start = self.spirals[s_spiral].cartesian(self.sub_arcs[0,i,j])
            midpoint = self.spirals[j].cartesian(self.sub_arcs[1,i,j])
            end = self.spirals[e_spiral].cartesian(self.sub_arcs[2,i,j])
            layer = 'F.Cu' if self.sub_arcs[3,i,j] == 1 else 'B.Cu'
            footprint.append(kcmod.Arc(center=tuple(np.squeeze(find_arc_center(start, midpoint, end))),start=start, end=end, layer=layer, width=self.trace_width))

        for i, via in enumerate(self.via_locations):
            if i % 2 == 0:
                spiral = self.spirals[0]
            else:
                spiral = self.spirals[-1]

            loc = spiral.cartesian(via)

            footprint.append(kcmod.Pad(type=kcmod.Pad.TYPE_THT, shape=kcmod.Pad.SHAPE_CIRCLE, at=loc, layers=["F.Cu", "B.Cu"], drill=0.254, size=[0.508, 0.508]))

        file_handler = kcmod.KicadFileHandler(footprint)
        file_handler.writeFile(filename)

    def check_arc_length(self):
        """DO NOT USE -- Used during initial debug, now that arcs are subdivided will not calculate length of arc correctly """

        start_pts = []
        midpoints = []
        end_pts = []

        for i in range(self.sub_arcs.shape[1]):
            start_pts.append(self.spirals[self.middle_spiral_index].cartesian(self.sub_arcs[0,i,self.middle_spiral_index]))
            midpoints.append(self.spirals[self.middle_spiral_index].cartesian(self.sub_arcs[1,i,self.middle_spiral_index]))
            end_pts.append(self.spirals[self.middle_spiral_index].cartesian(self.sub_arcs[2,i,self.middle_spiral_index]))

        print(length_of_3point_arc(start_pts, midpoints, end_pts))

        

if __name__ == "__main__":

    litz = SpiralLitz(11, 5, 80, num_sections=300)
        
    litz.save("test")

    # litz.plot()