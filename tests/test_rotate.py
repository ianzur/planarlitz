
from planarlitz.geometry import rotate

import numpy as np


def test_rotate():

    pts = np.array([(1, 0), (0, -1)])
    expected = np.array([(0, 1), (1, 0)])

    res = rotate(pts, np.pi / 2)

    assert np.allclose(res, expected)
