# This file is part of Planar Litz Generator.
#
# PlanarLitzGenerator is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published 
# by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Planar Litz Generator is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Planar Litz Generator. If not, see <https://www.gnu.org/licenses/>.
# 
# (c) 2022 by Ian Zurutuza, <ian.zurutuza@gmail.com>

def main(args):

    raise NotImplementedError()


if __name__ == "__main__":

    parser = ArgumentParser(description="generate pcblitz inductor spirals")

    parser.add_argument(
        "-i", "--inner", type=float, help="inner diameter of spiral [mm]"
    )
    parser.add_argument(
        "-o", "--outer", type=float, default=80.0, help="outer diameter of spiral, [mm]"
    )
    parser.add_argument("-n", "--turns", type=int, default=6, help="number of turns")
    parser.add_argument("-t", "--traces", type=int, default=5, help="number of traces")
    parser.add_argument(
        "-w", "--trace_width", type=float, default=0.1, help="trace_width [mm]"
    )
    parser.add_argument(
        "-c",
        "--trace_clearance",
        type=float,
        default=0.05,
        help="clearance between traces [mm]",
    )
    parser.add_argument(
        "-tc",
        "--turn_clearance",
        type=float,
        default=0.2,
        help="clearance between traces [mm]",
    )
    ## I do not think this is required when using kicad's Arc element
    # parser.add_argument("-l", "--segment_length", type=float, help="length of segment used to approx circular arc, [microns, um]")
    parser.add_argument("--net", type=int, default=2, help="trace net number")

    args = parser.parse_args()

    # parser = ArgumentParser(description="generate pcb inductors")

    # parser.add_argument("-s", "--shape", type=str, choices=["rect", "hex", "cir"])
    # parser.add_argument("-o", "--outer_dim", type=float, default=80.0, help="outer dimension of inductor [mm]. if shape=\"rect\" [width [mm], height [mm]]")
    # parser.add_argument("-i", "--inner_dim", type=float, help="inner dimension of inductor [mm]. if shape=\"rect\" [mm, mm]")
    # parser.add_argument("-n", "--turns", type=int, default=6, help="number of turns")
    # parser.add_argument("-t", "--traces", type=int, default=3, help="number of traces")
    # parser.add_argument("-w", "--trace_width", type=float, default=0.5, help="trace_width [mm]")
    # parser.add_argument("-c", "--trace_pitch", type=float, default=1.0, help="pitch between traces [mm]")
    # parser.add_argument("-tc", "--turn_pitch", type=float, help="pitch between turns [mm]")

    # parser.add_argument("-r", "--round", type=bool, default=True, help="smooth corners")           
    # parser.add_argument("--net", type=int, default=2, help="trace net number")
    # parser.add_argument("--footprint", type=bool, default=False, help="generate a footprint file")

    # args = parser.parse_args()


    main(args)
