# This file is part of Planar Litz Generator.
#
# PlanarLitzGenerator is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published 
# by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Planar Litz Generator is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Planar Litz Generator. If not, see <https://www.gnu.org/licenses/>.
# 
# (c) 2022 by Ian Zurutuza, <ian.zurutuza@gmail.com>import numpy as np

from scipy.special import lambertw

from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt


# class Spirangle:

#     def __init__(self, phi, r):

#         self.phi = phi
#         self.r = r

#     def calc(self, n):

#         x = []
#         y = []

#         for i in range()




def main():

    theta = np.arange(0, 3*2*np.pi, np.pi / 16)
    
    a, b = 1, .5 / (2 * np.pi)

    r = (a + b * theta)

    # print(r_a[1] - r_a[0], r_b[3] - r_b[2])

    x = r * np.cos(-theta)
    y = r * np.sin(-theta)

    my_dpi = 96
    fig, axs = plt.subplots(1, 1, figsize=(800 / my_dpi, 800 / my_dpi), dpi=my_dpi)

    axs.set_xlim(-5,5)
    axs.set_ylim(-5,5)
    plt.plot(x, y)
  

    # thetas = np.array(list(calc_theta_for_consistent_arc_length(1, b)))
    # print(thetas)

    # r = (a + b * thetas)

    arr = np.arange(0,200,1)
    thetas = theta_equidistance(np.full_like(arr.shape, .75), arr)

    r = a + b * thetas

    x = r * np.cos(-thetas)
    y = r * np.sin(-thetas)

    # pts = np.array(list(archspiral2(5, a, b)))
    # print(pts)

    plt.scatter(x,y)
    # ax.set_title("spiral", va='bottom')
    plt.show()


def theta_equidistance(k, n):
    return np.sqrt(2) * np.sqrt(-1 + np.sqrt(1 + k ** 2 + n ** 2 ))



def calc_theta_for_consistent_arc_length(s, b, theta=0.001):
    
    while theta < (6 * np.pi):
    #     # yield np.sqrt(2) * np.sqrt(-1 + np.sqrt(1 + k**2 * n**2))
        theta += b / 2 * (s / (np.sqrt(1 + theta ** 2)))
        yield theta

def archspiral2(s, a, b):
    theta = 0.001
    while theta < (6 * np.pi):
        r = a + b * theta
        yield (r*theta*np.cos(-theta), r*theta*np.sin(-theta))
        theta += s/(r*np.sqrt(1 + theta**2))



def elliptical_frustum(at: float=1.0, bt: float=0.6, ab: float=3, bb: float=1.8, h: float=1.0):

    # ellipsoid formula
    # top = 
    # bottom = 

    fig = plt.figure()
    ax = plt.axes(projection="3d")

    theta = np.arange(0, 2*np.pi, np.pi / 16)
    z = np.linspace(0, h, num=200)

    xt = at * np.cos(theta)
    yt = bt * np.sin(theta)
    zt = np.repeat(h, theta.shape)

    ax.scatter3D(xt, yt, zt)

    xb = ab * np.cos(theta)
    yb = bb * np.sin(theta)
    zb = np.repeat(0, theta.shape)

    ax.scatter3D(xb, yb, zb)

    ax.set_xlim(-4, 4)
    ax.set_ylim(-4, 4)

    # x = ((a * (h - z) + b * z) / h) * np.cos(v)
    # y = ((b * (h - z) + a * z) / h) * np.sin(v)

    # for i in range(len(z)):
    #     # print(x[:, i].shape)
    #     ax.scatter3D(x[:,i],y[:, i],z[i].repeat(32))
   
    plt.show()


def conical_frustum(a,b,h):

    z = np.array(np.linspace(0, h, num=50))
    theta = np.array([np.arange(0, 2 * np.pi, np.pi / 16)]).T

    x = ( (a * (h - z) + b*z))/h * np.cos(theta)
    y = .5 * ((a * (h - z) + b*z))/h * np.sin(theta)

    fig = plt.figure()
    ax = plt.axes(projection="3d")

    ax.scatter3D(x, y, np.broadcast_to(z, (32, 50)))
    
    ax.set_xlim(-1, 1)
    ax.set_ylim(-1, 1)


    plt.show()

def elliptic_cone(a,b,h):

    u = np.linspace(0, h, 50)
    theta = np.arange(0, 2 * np.pi, np.pi / 16)

    z = u

    x = a * ( ( h - z ) / h ) * np.cos(theta) 
    y = b * ( ( h - z ) / h ) * np.sin(theta) 

    fig = plt.figure()
    ax = plt.axes(projection="3d")

    ax.scatter3D(x, y, z)
    
    plt.figure()


def polar():


    r = a + b * theta

if __name__ == "__main__":

    # elliptical_frustum()
    # conical_frustum(1, .5, 5)
    main()
    # polar()
