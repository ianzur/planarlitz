from setuptools import find_packages, setup

with open("README.md", 'r') as fh:
    long_description = fh.read()

setup(
    name='planarlitz',
    version='0.0.1',
    author='Ian Zurutuza',
    author_email='ian.zurutuza@gmail.com',
    description='Create litz style inductor footprints for Kicad',
    license="GNU General Public License v3",
    package_dir={'': 'planarlitz'},
    packages=find_packages(where='planarlitz'),
    long_description=long_description,
    long_description_content_type="text/markdown",
    install_requires=[
        "numpy",
        "scipy",
        "KicadModTree",
    ]
    project_urls={
        "GitLab": "",
        "Bug Tracker": "",
    },
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    ]
)
