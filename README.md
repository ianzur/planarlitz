# Planar Litz
![python versions](https://img.shields.io/pypi/pyversions/planarlitz)
![downloads](https://img.shields.io/pypi/dm/planarlitz)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

> :warning: This package is in alpha

This tool can be used to create litz style inductors as Kicad footprints.

Inspired by the following paper, but with several changes. Most notably:
- Spiral creation is based on an [Archimedean spiral](https://en.wikipedia.org/wiki/Archimedean_spiral), not a half circle spiral.
- Layout parameters cannot be varied per turn

> J. Serrano, I. Lope, J. Acero, C. Carretero and J. M. Burdío, "Mathematical description of PCB-adapted litz wire geometry for automated layout generation of WPT coils," IECON 2017 - 43rd Annual Conference of the IEEE Industrial Electronics Society, 2017, pp. 6955-6960, doi: 10.1109/IECON.2017.8217216.

### Install
1. Clone repository
2. `cd` into project directory
3. `pip install .`

### Usage

```python
import planarlitz
spiral = planarlitz.SpiralLitz(
    num_traces=11, 
    num_turns=5, 
    outer_diameter=80, 
    num_sections=200,
    )
spiral.save("example")
```

![example image](imgs/inductor.gif)

### TODOs
see: CONTRIBUTING.md
- command line interface
- python packaging
- testing & CI
- simulation (OpenEMS?)
