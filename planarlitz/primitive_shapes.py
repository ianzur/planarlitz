# This file is part of Planar Litz Generator.
#
# PlanarLitzGenerator is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published 
# by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# Planar Litz Generator is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with Planar Litz Generator. If not, see <https://www.gnu.org/licenses/>.
# 
# (c) 2022 by Ian Zurutuza, <ian.zurutuza@gmail.com>

import numpy as np
import numpy.typing as npt

import matplotlib.pyplot as plt

import KicadModTree as kcmod


class ArchimedeanSpiral:
    """archimedean spiral"""

    __slots__ = ("_a", "_b")

    def __init__(self, a: float, b: float):
        self._a = a
        self._b = b

    @property
    def a(self):
        return self._a

    @property
    def b(self):
        return self._b

    def r(self, theta):
        """calculate radius at theta"""
        return self._a + self._b * theta

    def cartesian(self, theta):

        r = self.r(theta)

        x = r * np.cos(theta)
        y = r * np.sin(theta)

        return (x, y)

    def points(self, num_turns: float, points_per_turn: int = 64) -> np.array:
        """_summary_

        Args:
            num_turns (float): how many turns
            points_per_turn (int, optional): number of points calculated to represent this spiral. Defaults to 64.

        Returns:
            np.array: points of spiral starting inside spiral
        """

        theta = np.arange(0, num_turns * 2 * np.pi + 0.001, 2 * np.pi / points_per_turn)

        r = self._a + self._b * theta

        x = r * np.cos(theta)
        y = r * np.sin(theta)

        points_cartesian = np.stack([x, y]).T

        return points_cartesian

    def arc_length(self, theta_a: float, theta_b: float):
        """calculate arc length from theta_a to theta_b

        Args:
            theta_a (float): radians, lower bound of integration
            theta_b (float): radians, upper bound of integration

        Returns:
            float: length of the arc from theta_a to theta_b

        Details:
            Arc length definition:
                https://en.wikipedia.org/wiki/Arc_length#Other_coordinate_systems

            nasty integration found using sagemath:
                https://www.sagemath.org/

            .. highlight:: bash

            ::
                sage: arclen, a, b, t, theta_a, theta_b = var('arclen a b t theta_a theta_b')
                sage: assume(a>0,b>0,theta_b-theta_a>0)
                sage: integral((b**2 + (a + b*t)^2)^0.5, t, theta_a, theta_b).simplify()

            .. math::
                -\frac{b^{2} \operatorname{arsinh}\left(\frac{b \theta_{a} + a}{b}\right) + \sqrt{b^{2} \theta_{a}^{2} + 2 \, a b \theta_{a} + a^{2} + b^{2}} {\left(b \theta_{a} + a\right)}}{2 \, b} + \frac{b^{2} \operatorname{arsinh}\left(\frac{b \theta_{b} + a}{b}\right) + \sqrt{b^{2} \theta_{b}^{2} + 2 \, a b \theta_{b} + a^{2} + b^{2}} {\left(b \theta_{b} + a\right)}}{2 \, b}

        """
        return (1 / (2 * self.b)) * (
            (
                self.b**2 * np.arcsinh((self.b * theta_b + self.a) / self.b)
                + (((self.a + self.b * theta_b) ** 2) + self.b**2) ** 0.5
                * (self.b * theta_b + self.a)
            )
            - (
                self.b**2 * np.arcsinh((self.b * theta_a + self.a) / self.b)
                + (((self.a + self.b * theta_a) ** 2) + self.b**2) ** 0.5
                * (self.b * theta_a + self.a)
            )
        )

    def as_footprint(
        self, num_turns: float = 4.0, start: float = 0, name: str = "spiral"
    ) -> kcmod.Footprint:

        points = self.points()

        return footprint


class Spirangle:
    def __init__(self, sides, exterior_height=80, exterior_width=50):

        self.sides = sides
        self.exterior_height = 80
        self.exterior_width = 50

    def points(self, num_turns):

        point = []

        return points


if __name__ == "__main__":
    pass
