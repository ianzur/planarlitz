Please raise an issue before developing to ensure work is not being duplicated and to keep everyone informed.

## Submitting merge requests 

1. Fork this repository.
1. Create a new branch. Don't use your fork's master branch
2. follow the code style (`black planar_litz_generator/ test/`)
3. Give your merge request a descriptive title

### settings 

Please configure your personal fork with the following settings:

<!-- 1. Settings->General->Visibility->Pipelines should be enabled and set to "Everyone with access". -->
<!-- 2. Settings->CI/CD->General Pipelines timeout should be set to 3 hours or longer -->
3. The "Allow commits from members who can merge to the target branch." option check box at the bottom of your merge request must be checked.
